#!/bin/sh

basedir="/opt/pinger"

script_inet_up="$basedir/scripts/inet-up"
script_inet_down="$basedir/scripts/inet-down"
script_host_up="$basedir/scripts/host-up"
script_host_down="$basedir/scripts/host-down"

monfile="$basedir/monitor"
deadfile="$basedir/dead_hosts"
newdeadsfile="$basedir/new_deads"

inetfile="$basedir/internet"
inetdeadflag="$basedir/inet_is_dead"

[ -d $basedir ] || {
	echo "base directory $basedir not found"
	exit 1
}
[ -f "$monfile" ] || {
	echo "file $monfile not found" >&2
	exit 1
}
[ -f "$deadfile" ] || touch $deadfile

# checking internet
DOWN=1
echo -n "checking internet connection... "
for host in `cat $inetfile | grep -Eo '^[^#]\S+'`; do
	host -W1 $host > /dev/null 2> /dev/null && ping -q -c2 -A -W500 -w1 $host > /dev/null 2> /dev/null
	[ $? -eq 0 ] && {
		DOWN=0
		if [ -f "$inetdeadflag" ]; then
			echo "up (was down)"
			rm $inetdeadflag
			# run inet-up script
			[ -x $script_inet_up ] && $script_inet_up
		else
			echo "up"
		fi
		break
	}
done
[ $DOWN -eq 1 ] && {
	if [ -f "$inetdeadflag" ]; then
		echo "down"
	else
		echo "down (was up)"
		touch $inetdeadflag
		# run inet-down script
		[ -x $script_inet_down ] && $script_inet_down
	fi
}

truncate -s0 $newdeadsfile
for host in `cat $monfile | grep -Eo '^[^#]\S+'`; do
	echo -n "checking host $host... "
	host -W1 $host > /dev/null 2> /dev/null && ping -q -c3 -A -W500 -w2 $host > /dev/null 2> /dev/null
	if [ $? -eq 0 ]; then
		# host is alive
		if grep -q "$host" $deadfile; then
			echo "alive (was dead)"
			# run host-up script
			[ -x $script_host_up ] && $script_host_up $host
		else
			echo "alive"
		fi
	else
		# host is dead
		if grep -q "$host" $deadfile; then
			echo "dead"
		else
			echo "dead (was alive)"
			# run host-down script
			[ -x $script_host_down ] && $script_host_down $host
		fi
		echo "$host" >> $newdeadsfile
	fi
done
mv $newdeadsfile $deadfile
